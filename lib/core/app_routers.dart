import 'package:donate_app/controllers/needs_details_controller.dart';
import 'package:donate_app/models/necessidade_model.dart';
import 'package:donate_app/pages/need_details_page.dart';
import 'package:donate_app/pages/edit_my_profile_page.dart';
import 'package:donate_app/pages/home_page.dart';
import 'package:donate_app/pages/my_profile_page.dart';
import 'package:donate_app/pages/onboarding_page.dart';
import 'package:donate_app/pages/product_registration_page.dart';
import 'package:donate_app/pages/profile_page.dart';
import 'package:donate_app/pages/sign_in_page.dart';
import 'package:donate_app/pages/sign_up_page.dart';
import 'package:donate_app/pages/welcome_page.dart';
import 'package:flutter/material.dart';

const String onboardingRoute = '/onboarging';
const String signInRoute = '/signIn';
const String signUpRoute = 'signUp';
const String welcomeRoute = '/welcome';
const String homeRoute = '/home';
const String donationDetailsRoute = '/donations/details';
const String profileRoute = '/profile';
const String myProfileRoute = '/myProfile';
const String editMyProfileRoute = '/editMyProfile';
const String productRegistrationRoute = 'productRegistration';

class AppRouters {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    final args = settings.arguments; // pegando os argumentos caso haja

    switch (settings.name) {
      case onboardingRoute:
        return MaterialPageRoute(builder: (_) => const OnBoardingPage());

      case signInRoute:
        return MaterialPageRoute(builder: (_) => const SignInPage());

      case signUpRoute:
        return MaterialPageRoute(builder: (_) => const SignUpPage());

      case welcomeRoute:
        return MaterialPageRoute(builder: (_) => const WelcomePage());

      case homeRoute:
        return MaterialPageRoute(builder: (_) => const HomePage());

      case donationDetailsRoute:
        if (args is DonationDetailsArgs) {
          return MaterialPageRoute(
            builder: (_) => NeedDetails(
              necessidadeModel: args.necessidadeModel,
              needsDetailsController: args.needsDetailsController,
            ),
          );
        }

        break;

      case profileRoute:
        return MaterialPageRoute(builder: (_) => const ProfilePage());

      case myProfileRoute:
        return MaterialPageRoute(builder: (_) => const MyProfilePage());

      case editMyProfileRoute:
        return MaterialPageRoute(builder: (_) => const EditMyProfilePage());

      case productRegistrationRoute:
        return MaterialPageRoute(builder: (_) => ProductRegistrationPage());

      default:
        return MaterialPageRoute(builder: (_) => const OnBoardingPage());
    }
  }
}

class DonationDetailsArgs {
  final NecessidadeModel necessidadeModel;
  final NeedsDetailsController needsDetailsController;

  const DonationDetailsArgs({
    required this.needsDetailsController,
    required this.necessidadeModel,
  });
}
