import 'package:flutter/material.dart';

class SignUpController {
  final _crtName = TextEditingController();
  final _crtlEmail = TextEditingController();
  final _crtlPassword = TextEditingController();
  final _crtlConfirmPassword = TextEditingController();
  final _crtPhone = TextEditingController();

  TextEditingController get crtName => _crtName;
  TextEditingController get crtlEmail => _crtlEmail;
  TextEditingController get crtlConfirmPassword => _crtlConfirmPassword;
  TextEditingController get crtlPassword => _crtlPassword;
  TextEditingController get crtPhone => _crtPhone;
}
