import 'package:flutter/material.dart';

class EditMyProfileController {
  // Informações Básicas
  final _crtName = TextEditingController();
  final _crtlEmail = TextEditingController();
  final _crtlPassword = TextEditingController();
  final _crtlConfirmPassword = TextEditingController();
  final _crtPhone = TextEditingController();

  TextEditingController get crtName => _crtName;
  TextEditingController get crtlEmail => _crtlEmail;
  TextEditingController get crtlConfirmPassword => _crtlConfirmPassword;
  TextEditingController get crtlPassword => _crtlPassword;
  TextEditingController get crtPhone => _crtPhone;

  // Informações de Endereco
  final _crtCep = TextEditingController();
  final _crtlogradouro = TextEditingController();
  final _crtBairro = TextEditingController();
  final _crtComplemento = TextEditingController();
  final _crtNumero = TextEditingController();
  final _crtCidade = TextEditingController();
  final _crtEstado = TextEditingController();

  TextEditingController get crtCep => _crtCep;
  TextEditingController get crtlogradouro => _crtlogradouro;
  TextEditingController get crtBairro => _crtBairro;
  TextEditingController get crtComplemento => _crtComplemento;
  TextEditingController get crtNumero => _crtNumero;
  TextEditingController get crtCidade => _crtCidade;
  TextEditingController get crtEstado => _crtEstado;

  bool valid() {
    return true;
  }
}
