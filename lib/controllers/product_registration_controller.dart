import 'package:flutter/widgets.dart';

class ProductRegistrationController {
  final _crtlQtd = TextEditingController();

  List<String> products = [
    'Água',
    'Biscoito',
    'Café',
  ];

   List<String> category = [
    'Vestuário',
    'Construção Civil',
    'Não Perecíveis',
  ];

  ValueNotifier<String?> selectedValueProduct = ValueNotifier<String?>(null);  
  ValueNotifier<String?> selectedValueCategory = ValueNotifier<String?>(null);


  ValueNotifier<int> itemQtdValue = ValueNotifier<int>(0);

  set selectedItemProdut(String? item) => selectedValueProduct.value = item;
    set selectedItemCategory(String? item) => selectedValueCategory.value = item;

  set itemQtd(int qtd) => itemQtdValue.value = qtd;

  String? get selectedItemProdut => selectedValueProduct.value;
    String? get selectedItemCategory => selectedValueCategory.value;

  int get itemQtd => itemQtdValue.value;

  TextEditingController get crtlQtd => _crtlQtd;
}
