import 'dart:developer';

import 'package:donate_app/dto/necessidade_dto.dart';
import 'package:donate_app/models/categoria_model.dart';
import 'package:donate_app/models/doacao_model.dart';
import 'package:donate_app/models/necessidade_model.dart';
import 'package:donate_app/repository/categoria_repository.dart';
import 'package:donate_app/repository/needs_repository.dart';
import 'package:flutter/cupertino.dart';

class NeedsController {
  // auth
  int userId = 1;

  // lista de Necessidades

  NeedsController() {
    _initializer();
  }

  ValueNotifier<bool> showAllNotifier = ValueNotifier<bool>(true);
  ValueNotifier<List<NecessidadeModel>> listNeedsNotifier =
      ValueNotifier<List<NecessidadeModel>>([]);

  set _showAll(bool show) => showAllNotifier.value = show;
  set _listNeeds(List<NecessidadeModel> necessidades) =>
      listNeedsNotifier.value = necessidades;

  bool get showAll => showAllNotifier.value;
  List<NecessidadeModel> get listNeeds => listNeedsNotifier.value;

  final List<CategoriaModel> categoriaModel =
      CategoriaRepository.getAllCategoria();

  List<NecessidadeModel> get getNeeds => showAll ? _allNeeds : _myNeeds;

  List<NecessidadeModel> get _myNeeds =>
      _allNeeds.where((n) => n.usuarioId == userId).toList();

  List<NecessidadeModel> get _allNeeds => NeedsRepository.getAllNeeds();

  void _initializer() {
    _listNeeds = getNeeds;
  }

  void updateShowAll(bool show) {
    _showAll = show;

    _listNeeds = getNeeds;
  }
}
