import 'dart:developer';

import 'package:flutter/cupertino.dart';

class NeedsDetailsController {
  final int qtd;

  NeedsDetailsController({required this.qtd});

  late ValueNotifier<int> necessidadeQtdNotifier = ValueNotifier<int>(qtd);

  get necessidadeqtd => necessidadeQtdNotifier.value;

  set _necessidadeQtd(int qtd) => necessidadeQtdNotifier.value = qtd;

  void incrementNeedsQtd() {
    necessidadeQtdNotifier.value++;
  }

  void decrementNeedsQtd() {
    necessidadeQtdNotifier.value--;
  }
}
