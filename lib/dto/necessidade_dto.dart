import 'package:donate_app/models/categoria_model.dart';
import 'package:donate_app/models/status_doacao_model.dart';
import 'package:donate_app/models/usuario_model.dart';

class NecessidadeDto {
  final int id;
  final int quantidade;
  final CategoriaModel categoria;
  final UsuarioModel usuario;
  final StatusDoacaoModel statusDoacao;
  final DateTime dataDeCadastro;

  const NecessidadeDto({
    required this.id,
    required this.quantidade,
    required this.categoria,
    required this.usuario,
    required this.statusDoacao,
    required this.dataDeCadastro,
  });
}
