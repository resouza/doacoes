import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:flutter/material.dart';

class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  final String title = 'Comece doando Agora';
  final String subtitle = 'Uma família ou abrigo precisa da sua ajuda';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          fit: StackFit.loose,

          clipBehavior: Clip.none, // This is what you need.
          children: [
            Image.asset(
              'assets/images/mulher.jpg',
              color: Colors.white.withOpacity(0.3),
              colorBlendMode: BlendMode.modulate,
              fit: BoxFit.fill,
              height: 600,
            ),
            Positioned(
              bottom: -10,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      title,
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                        color: Colors.black87,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text(
                        subtitle,
                        style: const TextStyle(fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 15),
          child: NextButtonComponent.primary(
            label: 'Continuar',
            onTap: () {
              Navigator.pushNamed(context, signUpRoute);
            },
          ),
        ),
      ),
    );
  }
}
