import 'package:donate_app/components/custom_form_field.dart';
import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/sign_bottom_component.dart';
import 'package:donate_app/components/sign_header_component.dart';
import 'package:donate_app/controllers/sign_in_controller.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:flutter/material.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _formKey = GlobalKey<FormState>();
  final sigInController = SignInController();
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SignHeader(
      titlePage: 'Entrar',
      child: Column(
        children: [
          const SizedBox(height: 20),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomFormField(
                  headingText: "Email",
                  hintText: "Email",
                  obsecureText: false,
                  suffixIcon: const SizedBox(),
                  controller: sigInController.crtlEmail,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.emailAddress,
                ),
                const SizedBox(height: 20),
                Column(
                  children: [
                    CustomFormField(
                      headingText: "Senha",
                      maxLines: 1,
                      textInputAction: TextInputAction.done,
                      textInputType: TextInputType.text,
                      hintText: "No mínimo 8 caracteres",
                      obsecureText: true,
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.visibility),
                        onPressed: () {},
                      ),
                      controller: sigInController.crtlPassword,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Checkbox(
                              checkColor: Colors.white,
                              fillColor: MaterialStateProperty.all(
                                  AppColors.primaryColor),
                              value: isChecked,
                              onChanged: (bool? value) {
                                setState(() {
                                  isChecked = value!;
                                });
                              },
                            ),
                            const Text('Lembre-se de mim'),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(
                              vertical: 16, horizontal: 20),
                          child: InkWell(
                            onTap: () {},
                            child: Text(
                              "Forgot Password?",
                              style: TextStyle(
                                  color: AppColors.blue.withOpacity(0.7),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                NextButtonComponent.primary(
                  label: 'Confirmar',
                  onTap: () {
                    // if (!_formKey.currentState!.validate()) return;

                    Navigator.pushNamed(context, homeRoute);
                  },
                ),
                SignBottomComponent(
                  titleBottom: 'Não tem uma conta? ',
                  subtitleBottom: 'Cadastre-se',
                  redirect: () {
                    Navigator.pushNamed(context, signUpRoute);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
