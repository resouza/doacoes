import 'package:donate_app/components/custom_form_field.dart';
import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/sign_bottom_component.dart';
import 'package:donate_app/components/sign_header_component.dart';
import 'package:donate_app/controllers/sign_up_controller.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:flutter/material.dart';

/// TODO
/// - Adionar Máscara para Telefone

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();

  final signUpController = SignUpController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SignHeader(
      titlePage: 'Cadastre-se',
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            const SizedBox(height: 20),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomFormField(
                  headingText: "Nome completo",
                  hintText: "Nome completo",
                  obsecureText: false,
                  suffixIcon: const SizedBox(),
                  controller: signUpController.crtName,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.name,
                ),
                const SizedBox(height: 20),
                CustomFormField(
                  headingText: "Email",
                  hintText: "Email",
                  obsecureText: false,
                  suffixIcon: const SizedBox(),
                  controller: signUpController.crtlEmail,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.emailAddress,
                ),
                const SizedBox(height: 20),
                CustomFormField(
                  headingText: "Telefone",
                  hintText: "Telefone",
                  obsecureText: false,
                  suffixIcon: const SizedBox(),
                  controller: signUpController.crtPhone,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.phone,
                ),
                const SizedBox(height: 20),
                CustomFormField(
                  headingText: "Senha",
                  hintText: "No mínimo 8 caracteres",
                  obsecureText: true,
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.visibility),
                    onPressed: () {},
                  ),
                  controller: signUpController.crtlPassword,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.visiblePassword,
                ),
                const SizedBox(height: 20),
                CustomFormField(
                  headingText: "Confirmar Senha",
                  hintText: "No mínimo 8 caracteres",
                  obsecureText: true,
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.visibility),
                    onPressed: () {},
                  ),
                  controller: signUpController.crtlConfirmPassword,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  textInputType: TextInputType.visiblePassword,
                ),
              ],
            ),
            Column(
              children: [
                const SizedBox(height: 30),
                Row(
                  children: [
                    Expanded(
                      child: NextButtonComponent.primary(
                        label: 'Confirmar',
                        onTap: () {
                          if (!_formKey.currentState!.validate()) return;

                          Navigator.pushNamed(context, homeRoute);
                        },
                      ),
                    ),
                  ],
                ),
                SignBottomComponent(
                  titleBottom: 'Já tem uma conta? ',
                  subtitleBottom: 'Inscreva-se',
                  redirect: () {
                    Navigator.pushNamed(context, signInRoute);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
