import 'package:donate_app/components/doacoes_card_component.dart';
import 'package:donate_app/controllers/donations_controller.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:flutter/material.dart';

// Doações do usuário
class DonationsPage extends StatelessWidget {
  final DonationsController donationsController;

  const DonationsPage({
    Key? key,
    required this.donationsController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return Padding(
    //   padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
    //   child: Column(
    //     children: [
    //       Expanded(
    //         child: ListView.separated(
    //           itemCount: donationsController.doacoes.length,
    //           shrinkWrap: true,
    //           separatorBuilder: (BuildContext context, int index) =>
    //               const Divider(),
    //           itemBuilder: (_, index) {
    //             final posts = donationsController.doacoes;

    //             return DoacoesCardComponent(
    //               produto: posts[index].descricao,
    //               categoria: 'ALIMENTO PERECÍVEL',
    //               dataDeCadastro: posts[index].dataDeCadastro,
    //               quantidadeProduto: posts[index].quantidade,
    //               statusDoacao: Colors.green,
    //               postador: 'Bruno Oliveira',
    //               onTap: () => Navigator.pushNamed(
    //                 context,
    //                 donationDetailsRoute,
    //                 arguments: DonationDetailsArgs(
    //                   necessidadeModel: posts[index].id,
    //                 ),
    //               ),
    //             );
    //           },
    //         ),
    //       ),
    //     ],
    //   ),
    // );

    return Container();
  }
}
