import 'package:donate_app/components/custom_card_component.dart';
import 'package:donate_app/components/doacoes_card_component.dart';
import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/profile_card_component.dart';
import 'package:donate_app/components/profile_item_component.dart';
import 'package:donate_app/components/round_icon_button_component.dart';
import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/controllers/needs_details_controller.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:donate_app/models/necessidade_model.dart';
import 'package:donate_app/models/usuario_model.dart';
import 'package:donate_app/repository/categoria_repository.dart';
import 'package:donate_app/repository/usuario_repository.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class NeedDetails extends StatefulWidget {
  final NecessidadeModel necessidadeModel;
  final NeedsDetailsController needsDetailsController;

  const NeedDetails({
    required this.necessidadeModel,
    required this.needsDetailsController,
    Key? key,
  }) : super(key: key);

  @override
  State<NeedDetails> createState() => _NeedDetailsState();
}

class _NeedDetailsState extends State<NeedDetails> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Detalhes da Necessidade'),
          backgroundColor: AppColors.primaryColor,
          foregroundColor: Colors.white,
          elevation: 0,
        ),
        backgroundColor: AppColors.primaryColor,
        body: RoundedBackgroundComponent(
          height: MediaQuery.of(context).size.height * 0.02,
          child: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.8,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ProfileCardComponent(
                        title: 'Informações Básicas',
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            DoacoesCardComponent(
                              produto: widget.necessidadeModel.descricao,
                              categoria: _getCategoriaNome(
                                  widget.necessidadeModel.categoriaId),
                              dataDeCadastro:
                                  widget.necessidadeModel.dataDeCadastro,
                              quantidadeProduto:
                                  widget.necessidadeModel.quantidade,
                              postador: _getUsuarioNome(
                                  widget.necessidadeModel.usuarioId),
                            ),
                          ],
                        ),
                      ),
                      ProfileCardComponent(
                        title: 'Informações do Usuário',
                        child: UserInfoCardComponent(
                          usuario: UsuarioRepository.getUserById(
                            widget.necessidadeModel.usuarioId,
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 20),
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Text(
                                  'Quantidade que quero doar',
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 15),
                              width: 120,
                              child: ChangeNeedsQtdComponent(
                                needsDetailsController:
                                    widget.needsDetailsController,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: NextButtonComponent.primary(
                      label: 'Confirmar doação',
                      onTap: () {
                        // if (!_formKey.currentState!.validate()) return;

                        Navigator.pushNamed(context, homeRoute);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String _getCategoriaNome(int categoriaId) {
    return CategoriaRepository.getCategoriaById(categoriaId).nome;
  }

  String _getUsuarioNome(int usuarioId) {
    return UsuarioRepository.getUserById(usuarioId).nome;
  }
}

class ChangeNeedsQtdComponent extends StatelessWidget {
  final NeedsDetailsController needsDetailsController;

  const ChangeNeedsQtdComponent({
    Key? key,
    required this.needsDetailsController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<int>(
      valueListenable: needsDetailsController.necessidadeQtdNotifier,
      builder: (context, qtd, _) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RoundIconButtonComponent(
              icon: FontAwesomeIcons.minus,
              onTap: needsDetailsController.decrementNeedsQtd,
            ),
            Text(
              '$qtd',
              style: const TextStyle(
                fontSize: 18,
              ),
            ),
            RoundIconButtonComponent(
              icon: FontAwesomeIcons.plus,
              onTap: needsDetailsController.incrementNeedsQtd,
            ),
          ],
        );
      },
    );
  }
}

class UserInfoCardComponent extends StatelessWidget {
  final UsuarioModel usuario;

  const UserInfoCardComponent({
    Key? key,
    required this.usuario,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardComponent(
      icon: Icons.person,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                usuario.nome,
                style: const TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Email: ${usuario.email}'),
              Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: usuario.ativo ? Colors.green : Colors.red,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('Telefone: ${usuario.telefone}'),
            ],
          ),
          Row(
            children: [
              Text(
                  'Usuário desde: ${DateFormat("dd-MM-yyyy").format(usuario.dataCadastro)}')
            ],
          )
        ],
      ),
    );
  }
}

class PersonalInfoItem extends StatelessWidget {
  final String title;
  final String subtitle;

  const PersonalInfoItem({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('$title:'),
        Text(subtitle),
      ],
    );
  }
}
