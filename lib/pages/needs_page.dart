import 'dart:developer';

import 'package:donate_app/components/doacoes_card_component.dart';
import 'package:donate_app/controllers/donations_controller.dart';
import 'package:donate_app/controllers/needs_controller.dart';
import 'package:donate_app/controllers/needs_details_controller.dart';
import 'package:donate_app/models/categoria_model.dart';
import 'package:donate_app/models/necessidade_model.dart';
import 'package:donate_app/repository/categoria_repository.dart';
import 'package:donate_app/repository/usuario_repository.dart';
import 'package:flutter/material.dart';
import 'package:donate_app/core/app_routers.dart';

// Doações dos outros - doações disponíveis
class NeedsPage extends StatefulWidget {
  final NeedsController needsController;

  const NeedsPage({
    Key? key,
    required this.needsController,
  }) : super(key: key);

  @override
  State<NeedsPage> createState() => _NeedsPageState();
}

class _NeedsPageState extends State<NeedsPage> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: Column(
        children: [
          Builder(builder: (context) {
            return _customSwitchListTile(widget.needsController);
          }),
          _customListView(widget.needsController),
        ],
      ),
    );
  }

  Widget _customSwitchListTile(NeedsController controller) {
    return ValueListenableBuilder<bool>(
      valueListenable: controller.showAllNotifier,
      builder: (context, showAll, _) {
        return SwitchListTile(
          title: const Text(
            'Mostrar todos',
            style: TextStyle(fontWeight: FontWeight.w400),
          ),
          // secondary: Icon(Icons.group_add),
          value: showAll,
          onChanged: (bool show) {
            controller.updateShowAll(show);
          },
        );
      },
    );
  }

  Widget _customListView(NeedsController needsController) {
    return ValueListenableBuilder<List<NecessidadeModel>>(
      valueListenable: needsController.listNeedsNotifier,
      builder: (context, listNeeds, _) {
        return Expanded(
          child: ListView.separated(
            itemCount: listNeeds.length,
            shrinkWrap: true,
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
            itemBuilder: (_, index) {
              NecessidadeModel necessidade = listNeeds[index];

              return DoacoesCardComponent(
                produto: necessidade.descricao,
                categoria: _getCategoriaNome(necessidade.categoriaId),
                dataDeCadastro: necessidade.dataDeCadastro,
                quantidadeProduto: necessidade.quantidade,
                postador: _getUsuarioNome(necessidade.usuarioId),
                onTap: () => Navigator.pushNamed(
                  context,
                  donationDetailsRoute,
                  arguments: DonationDetailsArgs(
                      necessidadeModel: necessidade,
                      needsDetailsController:
                          NeedsDetailsController(qtd: necessidade.quantidade)),
                ),
              );
            },
          ),
        );
      },
    );
  }

  String _getCategoriaNome(int categoriaId) {
    return CategoriaRepository.getCategoriaById(categoriaId).nome;
  }

  String _getUsuarioNome(int usuarioId) {
    return UsuarioRepository.getUserById(usuarioId).nome;
  }
}
