import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/controllers/donations_controller.dart';
import 'package:donate_app/controllers/needs_controller.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:donate_app/pages/needs_page.dart';
import 'package:donate_app/pages/donations_page.dart';
import 'package:donate_app/pages/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:donate_app/controllers/home_controller.dart';
import 'package:donate_app/core/app_colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final homeController = HomeController();
  final donationsController = DonationsController();
  final needsController = NeedsController();

  final _currentIndex = ValueNotifier<int>(0);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: _currentIndex,
        builder: (_, index, __) {
          return SafeArea(
            child: Scaffold(
              backgroundColor: AppColors.primaryColor,
              appBar: AppBar(
                title: Text(_buildTitle(_currentIndex.value)),
                // actions: [
                //   IconButton(
                //       onPressed: () {}, icon: const Icon(Icons.notifications))
                // ],
                backgroundColor: AppColors.primaryColor,
                foregroundColor: Colors.white,
                elevation: 0,
                automaticallyImplyLeading: false,
              ),
              body: RoundedBackgroundComponent(
                height: MediaQuery.of(context).size.height * 0.02,
                child: _buildBody(
                  _currentIndex.value,
                ),
              ),
              floatingActionButton: FloatingActionButton(
                backgroundColor: AppColors.primaryColor,
                highlightElevation: 200,
                onPressed: () =>
                    Navigator.pushNamed(context, productRegistrationRoute),
                child: const FaIcon(FontAwesomeIcons.boxOpen),
              ),
              bottomNavigationBar: BottomNavigationBar(
                onTap: (index) => _currentIndex.value = index,
                currentIndex: _currentIndex.value,
                backgroundColor: AppColors.primaryColor,
                selectedItemColor: Colors.white,
                unselectedItemColor: Colors.black54,
                items: const [
                  BottomNavigationBarItem(
                      icon: Icon(Icons.volunteer_activism),
                      label: 'Necessidades'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.home), label: 'Doações'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.person), label: 'Perfil'),
                ],
              ),
            ),
          );
        });
  }

  Widget _buildBody(int index) {
    switch (index) {
      case 0:
        return NeedsPage(needsController: needsController);

      case 1:
        return DonationsPage(donationsController: donationsController);

      case 2:
        return const ProfilePage();

      default:
        return NeedsPage(needsController: needsController);
    }
  }

  String _buildTitle(int index) {
    switch (index) {
      case 0:
        return 'Necessidades';
      case 1:
        return 'Doações';

      case 2:
        return '';

      default:
        return 'Necessidades';
    }
  }
}
