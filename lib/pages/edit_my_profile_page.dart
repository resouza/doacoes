import 'package:donate_app/components/custom_form_field.dart';
import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/controllers/edit_my_profile_controller.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class EditMyProfilePage extends StatefulWidget {
  const EditMyProfilePage({Key? key}) : super(key: key);

  @override
  State<EditMyProfilePage> createState() => _EditMyProfilePageState();
}

class _EditMyProfilePageState extends State<EditMyProfilePage> {
  final _formKey = GlobalKey<FormState>();

  final editMyProfileController = EditMyProfileController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Editar Perfil'),
          backgroundColor: AppColors.primaryColor,
          foregroundColor: Colors.white,
          elevation: 0,
        ),
        backgroundColor: AppColors.primaryColor,
        body: RoundedBackgroundComponent(
          height: MediaQuery.of(context).size.height * 0.02,
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
              width: double.infinity,
              child: Column(
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Informações Básicas',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                            Container(
                              height: 500,
                              margin:
                                  const EdgeInsets.only(bottom: 10, top: 10),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  CustomFormField(
                                    headingText: "Nome completo",
                                    hintText: "Nome completo",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller: editMyProfileController.crtName,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                  CustomFormField(
                                    headingText: "Email",
                                    hintText: "Email",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlEmail,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.emailAddress,
                                  ),
                                  CustomFormField(
                                    headingText: "Telefone",
                                    hintText: "Telefone",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtPhone,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.phone,
                                  ),
                                  CustomFormField(
                                    headingText: "Senha",
                                    hintText: "No mínimo 8 caracteres",
                                    obsecureText: true,
                                    suffixIcon: IconButton(
                                      icon: const Icon(Icons.visibility),
                                      onPressed: () {},
                                    ),
                                    controller:
                                        editMyProfileController.crtlEmail,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType:
                                        TextInputType.visiblePassword,
                                  ),
                                  CustomFormField(
                                    headingText: "Nova Senha",
                                    hintText: "No mínimo 8 caracteres",
                                    obsecureText: true,
                                    suffixIcon: IconButton(
                                      icon: const Icon(Icons.visibility),
                                      onPressed: () {},
                                    ),
                                    controller:
                                        editMyProfileController.crtlEmail,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType:
                                        TextInputType.visiblePassword,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const Divider(color: AppColors.black),
                        const SizedBox(height: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              'Endereço',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w500),
                            ),
                            Container(
                              height: 600,
                              margin:
                                  const EdgeInsets.only(bottom: 10, top: 10),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  CustomFormField(
                                    headingText: "CEP",
                                    hintText: "CEP",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller: editMyProfileController.crtCep,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.number,
                                  ),
                                  CustomFormField(
                                    headingText: "Logradouro",
                                    hintText: "Logradouro",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                  CustomFormField(
                                    headingText: "Bairro",
                                    hintText: "Bairro",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                  CustomFormField(
                                    headingText: "Complemento",
                                    hintText: "Complemento",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                  CustomFormField(
                                    headingText: "Número",
                                    hintText: "Número",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.number,
                                  ),
                                  CustomFormField(
                                    headingText: "Cidade",
                                    hintText: "Cidade",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                  CustomFormField(
                                    headingText: "Estado",
                                    hintText: "Estado",
                                    obsecureText: false,
                                    suffixIcon: const SizedBox(),
                                    controller:
                                        editMyProfileController.crtlogradouro,
                                    maxLines: 1,
                                    textInputAction: TextInputAction.done,
                                    textInputType: TextInputType.name,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 40),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: NextButtonComponent.primary(
                                label: 'Confirmar',
                                onTap: () {
                                  if (editMyProfileController.valid()) {
                                    Navigator.pop(context);
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                        const SizedBox(height: 8),
                        Row(
                          children: [
                            Expanded(
                              child: NextButtonComponent.white(
                                label: 'Cancelar',
                                onTap: () {
                                  Navigator.pop(context);
                                },
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//