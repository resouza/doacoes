import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/profile_card_component.dart';
import 'package:donate_app/components/profile_item_component.dart';
import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:donate_app/core/app_text_styles.dart';
import 'package:flutter/material.dart';

class MyProfilePage extends StatelessWidget {
  const MyProfilePage({Key? key}) : super(key: key);

  final String imageNetwork =
      'https://static.jobscan.co/blog/uploads/hickman-testimonial-1.png';
  final String personName = 'Gilvan Anjos';
  final String personEMail = 'gilvananjos10ns@gmail.com';
  final String personCpf = '03881913092';
  final String personBirthday = '04/10/1993';

  final String personAddres =
      'R Euclenice S Nascimento, 24, Casa no beco com grades pretas , Itinga Lauro de Freitas, Bahia, 42738690';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Meu Perfil'),
          backgroundColor: AppColors.primaryColor,
          foregroundColor: Colors.white,
          elevation: 0,
        ),
        backgroundColor: AppColors.primaryColor,
        body: RoundedBackgroundComponent(
          height: MediaQuery.of(context).size.height * 0.02,
          child: SingleChildScrollView(
            child: SizedBox(
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30, vertical: 20),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 20),
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(imageNetwork),
                            minRadius: 60,
                            maxRadius: 60,
                          ),
                        ),
                        Column(
                          children: [
                            Text(
                              personName.toUpperCase(),
                              style: AppTextStyles.textFieldHeading,
                            ),
                            const SizedBox(height: 10),
                            Text(
                              personEMail.toUpperCase(),
                              style: const TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      const Divider(color: AppColors.black),
                      ProfileCardComponent(
                        title: 'Informações Básicas',
                        child: Column(
                          children: [
                            ProfileItemComponent(
                              title: 'NOME',
                              subtitle: personName,
                              onTap: () {},
                            ),
                            const SizedBox(height: 10),
                            ProfileItemComponent(
                              title: 'CPF / CPNJ',
                              subtitle: personCpf,
                              onTap: () {},
                            ),
                            const SizedBox(height: 10),
                            ProfileItemComponent(
                              title: 'NASCIMENTO',
                              subtitle: personBirthday,
                              onTap: () {},
                            ),
                          ],
                        ),
                      ),
                      const Divider(color: AppColors.secondaryColor),
                      ProfileCardComponent(
                        title: 'Endereço',
                        child: Text(personAddres),
                      ),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 40),
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      children: [
                        Expanded(
                          child: NextButtonComponent.primary(
                            label: 'Editar',
                            onTap: () => Navigator.pushNamed(
                                context, editMyProfileRoute),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
