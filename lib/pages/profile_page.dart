import 'package:donate_app/components/custom_leading_profile_component.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:donate_app/core/app_text_styles.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  final String imageNetwork =
      'https://static.jobscan.co/blog/uploads/hickman-testimonial-1.png';

  final String personName = 'Gilvan Anjos';

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        child: Column(
          children: [
            Column(
              children: [
                CircleAvatar(
                  backgroundImage: NetworkImage(imageNetwork),
                  minRadius: 80,
                  maxRadius: 80,
                ),
                Text(
                  personName.toUpperCase(),
                  style: AppTextStyles.textFieldHeading,
                ),
              ],
            ),
            const SizedBox(height: 20),
            CustomLeadingProfileComponent(
              title: 'MEU PERFIL',
              icon: Icons.person,
              onTap: () {
                Navigator.pushNamed(context, myProfileRoute);
              },
            ),
            CustomLeadingProfileComponent(
              title: 'CONFIGURAÇÕES',
              icon: Icons.settings,
              onTap: () {},
            ),
            CustomLeadingProfileComponent(
              title: 'SOBRE',
              icon: Icons.info,
              onTap: () {},
            ),
            CustomLeadingProfileComponent(
              title: 'SAIR',
              icon: Icons.exit_to_app,
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}
