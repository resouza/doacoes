import 'dart:developer';

import 'package:donate_app/components/next_button_component.dart';
import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/controllers/product_registration_controller.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_routers.dart';
import 'package:donate_app/core/app_text_styles.dart';
import 'package:flutter/material.dart';

// Vestuário
// Construcao
// Não perecíveis
class ProductRegistrationPage extends StatelessWidget {
  ProductRegistrationPage({Key? key}) : super(key: key);

  final ProductRegistrationController _controller =
      ProductRegistrationController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registro de Produto'),
        backgroundColor: AppColors.primaryColor,
        foregroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: AppColors.primaryColor,
      body: RoundedBackgroundComponent(
        height: MediaQuery.of(context).size.height * 0.02,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                DonationItemComponent(
                  productRegistrationController: _controller,
                ),
                Row(
              children: [
                Expanded(
                  child: NextButtonComponent.primary(
                    label: 'Confirmar',
                    onTap: () {
                      Navigator.pushNamed(context, homeRoute);
                    },
                  ),
                ),
              ],
            ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DonationItemComponent extends StatelessWidget {
  final ProductRegistrationController productRegistrationController;

  const DonationItemComponent({
    Key? key,
    required this.productRegistrationController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: Listenable.merge([
                productRegistrationController.selectedValueCategory,

        productRegistrationController.selectedValueProduct,
        productRegistrationController.itemQtdValue
      ]),
      builder: (_, __) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [            
            const Text('Categoria'),
            CustomDropDownButtonComponent(
              selected: productRegistrationController.selectedValueCategory.value,
              items: productRegistrationController.category,
              hint: 'Selecione uma opção',
              onChanged: (item) =>
                  productRegistrationController.selectedItemCategory = item,
            ),

            const Text('Produto'),
            CustomDropDownButtonComponent(
              selected: productRegistrationController.selectedValueProduct.value,
              items: productRegistrationController.products,
              hint: 'Selecione uma opção',
              onChanged: (item) =>
                  productRegistrationController.selectedItemProdut = item,
            ),
            const SizedBox(height: 20),
            const Text('Quantidade'),
            TextField(
              maxLines: 1,
              controller: productRegistrationController.crtlQtd,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                hintStyle: AppTextStyles.textFieldHintStyle,
                border: InputBorder.none,
              ),
            ),
            const SizedBox(height: 40),
            
          ],
        );
      },
    );
  }
}

class CustomDropDownButtonComponent extends StatelessWidget {
  final String? selected;
  final List<String?> items;
  final String hint;
  final void Function(String?)? onChanged;

  const CustomDropDownButtonComponent({
    Key? key,
    required this.selected,
    required this.items,
    required this.onChanged,
    required this.hint,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String?>(
      value: selected,
      hint: Text(hint),
      items: items
          .map((item) => DropdownMenuItem<String?>(
                value: item,
                child: Text(
                  item!,
                  style: const TextStyle(fontSize: 24),
                ),
              ))
          .toList(),
      onChanged: onChanged,
    );
  }
}
