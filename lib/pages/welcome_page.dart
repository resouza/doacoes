import 'package:donate_app/pages/sign_in_page.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SignInPage();

    // SignComponent(
    //   titlePage: 'Bem-vindo de volta',
    //   googleMessage: 'Se conecte usando o Google',
    //   facebookMessage: 'Se conecte usando o Facebook',
    //   emailMessage: 'Se conecte usando o Email',
    //   googleOnTap: () {},
    //   facebookOnTap: () {},
    //   emailOnTap: () {},
    //   titleBottom: 'Não tem uma conta? ',
    //   subtitleBottom: 'Inscreva-se',
    //   redirect: () {
    //     Navigator.pushReplacementNamed(context, signUpRoute);
    //   },
    // );
  }
}
