import 'package:donate_app/components/rounded_background_component.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_images.dart';
import 'package:flutter/material.dart';

class SignHeader extends StatefulWidget {
  final String titlePage;
  final Widget child;

  const SignHeader({
    required this.titlePage,
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<SignHeader> createState() => _SignHeaderState();
}

class _SignHeaderState extends State<SignHeader> {
  @override
  Widget build(BuildContext context) {
    // return

    // SafeArea(
    //     child: Scaffold(
    //   body: SingleChildScrollView(
    //     child: Container(
    //       padding: const EdgeInsets.symmetric(horizontal: 10),
    //       height: height,
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.center,
    //         children: [
    //           Container(
    //             margin: const EdgeInsets.symmetric(vertical: 50),
    //             height: 200,
    //             width: 150,
    //             decoration: BoxDecoration(
    //               image: DecorationImage(
    //                 image: AssetImage(AppImages.logoDelivery),
    //                 fit: BoxFit.fill,
    //               ),
    //             ),
    //           ),
    //           Align(
    //             alignment: Alignment.centerLeft,
    //             child: Text(
    //               titlePage,
    //               style: const TextStyle(fontSize: 28, color: AppColors.orange),
    //             ),
    //           ),
    //           Container(
    //             margin: const EdgeInsets.only(top: 20),
    //             child: child,
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    // ));

    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.primaryColor,
        body: RoundedBackgroundComponent(
          height: MediaQuery.of(context).size.height * 0.02,
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  headerImage(),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.titlePage,
                      style: const TextStyle(
                          fontSize: 28, color: AppColors.primaryColor),
                    ),
                  ),
                  widget.child,
                ],
              ),
            ),
          ),
        ),
      ),
    );

    // return SafeArea(
    //     child: Column(
    //   children: [
    //     Expanded(
    //         child: Stack(
    //       children: [
    //         Container(
    //           margin: EdgeInsets.only(top: widget.height * 0.13),
    //           decoration: const BoxDecoration(
    //             color: AppColors.whiteshade,
    //             borderRadius: BorderRadius.only(
    //               topLeft: Radius.circular(40),
    //               topRight: Radius.circular(40),
    //             ),
    //           ),
    //         ),
    //         Padding(
    //           padding: const EdgeInsets.only(top: 20),
    //           child: widget.child,
    //         ),
    //       ],
    //     ))
    //   ],
    // ));
  }

  Widget headerImage() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 50),
      height: 200,
      width: 200,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.login),
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
