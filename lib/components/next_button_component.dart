import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class NextButtonComponent extends StatelessWidget {
  final String label;
  final Color backgroundColor;
  final Color fontColor;
  final Color borderColor;
  final VoidCallback onTap;

  NextButtonComponent({
    required this.label,
    required this.backgroundColor,
    required this.fontColor,
    required this.borderColor,
    required this.onTap,
  });

  // Contrutores nomeados
  NextButtonComponent.primary(
      {required String label, required VoidCallback onTap})
      : this.backgroundColor = AppColors.primaryColor,
        this.fontColor = Colors.white,
        this.label = label,
        this.borderColor = AppColors.darkGreen,
        this.onTap = onTap;

  NextButtonComponent.white(
      {required String label, required VoidCallback onTap})
      : this.backgroundColor = AppColors.white,
        this.fontColor = AppColors.grey,
        this.label = label,
        this.borderColor = AppColors.border,
        this.onTap = onTap;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 48,
            child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(backgroundColor),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10))),
                // side: MaterialStateProperty.all(BorderSide(color: borderColor)),
              ),
              onPressed: onTap,
              child: Text(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: fontColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
