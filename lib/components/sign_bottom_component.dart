import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class SignBottomComponent extends StatelessWidget {
  final String titleBottom;
  final String subtitleBottom;
  final VoidCallback redirect;

  const SignBottomComponent({
    Key? key,
    required this.titleBottom,
    required this.subtitleBottom,
    required this.redirect,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            titleBottom,
            style: const TextStyle(fontSize: 16, color: Colors.black),
          ),
          GestureDetector(
            child: Text(
              subtitleBottom,
              style:
                  const TextStyle(fontSize: 16, color: AppColors.primaryColor),
            ),
            onTap: redirect,
          ),
        ],
      ),
    );
  }
}
