import 'package:donate_app/components/social_media_button_component.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:donate_app/core/app_images.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignComponent extends StatelessWidget {
  const SignComponent({
    Key? key,
    required this.titlePage,
    required this.googleMessage,
    required this.facebookMessage,
    required this.emailMessage,
    required this.googleOnTap,
    required this.facebookOnTap,
    required this.emailOnTap,
    required this.titleBottom,
    required this.subtitleBottom,
    required this.redirect,
  }) : super(key: key);

  final String titlePage;

  final String googleMessage;
  final String facebookMessage;
  final String emailMessage;

  final VoidCallback googleOnTap;
  final VoidCallback facebookOnTap;
  final VoidCallback emailOnTap;

  final String titleBottom;
  final String subtitleBottom;

  final VoidCallback redirect;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 50),
                height: 200,
                width: 150,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(AppImages.logoAjudae),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  titlePage,
                  style: const TextStyle(fontSize: 28, color: AppColors.orange),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 60),
                height: 220,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SocialMediaButtonComponent(
                      icon: FontAwesomeIcons.google,
                      label: googleMessage,
                      onTap: googleOnTap,
                    ),
                    SocialMediaButtonComponent(
                      icon: FontAwesomeIcons.facebook,
                      label: facebookMessage,
                      onTap: facebookOnTap,
                    ),
                    SocialMediaButtonComponent(
                      icon: FontAwesomeIcons.envelope,
                      label: emailMessage,
                      onTap: emailOnTap,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      titleBottom,
                      style: const TextStyle(fontSize: 16, color: Colors.black),
                    ),
                    GestureDetector(
                      child: Text(
                        subtitleBottom,
                        style: const TextStyle(
                            fontSize: 16, color: AppColors.orange),
                      ),
                      onTap: redirect,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
