import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class SquareInputFieldComponent extends StatelessWidget {
  final String labelText;
  final ValueChanged<String>? onChanged;
  final TextEditingController controller;
  final TextInputType? keyboardType;

  const SquareInputFieldComponent({
    Key? key,
    required this.labelText,
    this.onChanged,
    required this.controller,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      onChanged: onChanged,
      controller: controller,
      validator: _validaCampo,
      cursorColor: AppColors.primaryColor,
      decoration: InputDecoration(
        labelText: labelText,
        border: const OutlineInputBorder(),
      ),
    );
  }

  String? _validaCampo(String? texto) =>
      (texto!.isEmpty) ? "Preencha o campo" : null;
}
