import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class CustomLeadingProfileComponent extends StatelessWidget {
  final String title;
  final IconData icon;
  final Function() onTap;

  const CustomLeadingProfileComponent({
    Key? key,
    required this.title,
    required this.icon,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        color: AppColors.secondaryColor,
        size: 25,
      ),
      title: Text(
        title,
        style: const TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontSize: 18,
        ),
      ),
      onTap: onTap,
    );
  }
}
