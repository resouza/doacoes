import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SocialMediaButtonComponent extends StatelessWidget {
  final VoidCallback onTap;
  final IconData icon;
  final String label;

  const SocialMediaButtonComponent({
    required this.onTap,
    required this.icon,
    required this.label,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black),
      ),
      child: ListTile(
        leading: FaIcon(icon, color: Colors.black),
        title: Text(
          label,
          style: const TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        onTap: onTap,
      ),

      // child: Row(
      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //   children: [
      //     Flexible(
      //         flex: 2,
      //         child: FaIcon(
      //           icon,
      //           color: Colors.black54,
      //         )),
      //     Flexible(
      //       flex: 4,
      //       child: Text(
      //         label,
      //         style: const TextStyle(
      //           fontSize: 18,
      //           color: Colors.black87,
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}
