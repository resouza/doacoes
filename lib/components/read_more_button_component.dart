import 'package:flutter/material.dart';

class ReadMoreButtonComponent extends StatelessWidget {
  final Function() onTap;

  const ReadMoreButtonComponent({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: const [
          Text('Informações'),
          Icon(Icons.chevron_right),
        ],
      ),
    );
  }
}
