import 'package:donate_app/components/custom_card_component.dart';
import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DoacoesCardComponent extends StatelessWidget {
  final String produto;
  final String categoria;
  final String postador;
  final DateTime dataDeCadastro;
  final Color? statusDoacao;
  final int quantidadeProduto;

  final void Function()? onTap;

  const DoacoesCardComponent({
    Key? key,
    required this.produto,
    required this.categoria,
    required this.postador,
    required this.dataDeCadastro,
    this.statusDoacao,
    required this.quantidadeProduto,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCardComponent(
      icon: Icons.restaurant,
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                produto,
                style: const TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(DateFormat("dd-MM-yyyy").format(dataDeCadastro))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(categoria),
              Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: statusDoacao,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('Postado por: $postador'),
            ],
          ),
          Row(
            children: [
              Text('Quantidade: $quantidadeProduto'),
            ],
          ),
        ],
      ),
    );
  }
}
