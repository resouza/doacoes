import 'package:donate_app/core/app_colors.dart';
import 'package:flutter/material.dart';

class CustomCardComponent extends StatelessWidget {
  final IconData icon;
  final void Function()? onTap;
  final Widget child;

  const CustomCardComponent({
    Key? key,
    this.onTap,
    required this.child,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          height: 90,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            children: [
              Icon(
                icon,
                size: 30,
                color: AppColors.primaryColor,
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: child,
                ),
              ),
              onTap != null ? const Icon(Icons.chevron_right) : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
