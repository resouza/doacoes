class StatusModel {
  final int id;
  final String descricao;

  const StatusModel({
    required this.id,
    required this.descricao,
  });
}
