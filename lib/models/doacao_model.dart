class DoacaoModel {
  final int id;
  final String descricao;
  final int quantidade;
  final int categoriaId;
  final int usuarioId;
  final int statusDoacaoId;
  final DateTime dataDeCadastro;

  DoacaoModel({
    required this.id,
    required this.descricao,
    required this.quantidade,
    required this.categoriaId,
    required this.usuarioId,
    required this.statusDoacaoId,
    required this.dataDeCadastro,
  });
}
