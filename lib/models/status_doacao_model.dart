class StatusDoacaoModel {
  final int id;
  final String descricao;

  StatusDoacaoModel({
    required this.id,
    required this.descricao,
  });
}
