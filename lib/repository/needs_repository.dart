import 'package:donate_app/models/necessidade_model.dart';

class NeedsRepository {
  static List<NecessidadeModel> getAllNeeds() {
    return doacoes;
  }
}

List<NecessidadeModel> doacoes = <NecessidadeModel>[
  NecessidadeModel(
    id: 1,
    descricao: 'Achocolatado',
    quantidade: 4,
    categoriaId: 1,
    usuarioId: 2,
    statusDoacaoId: 1,
    dataDeCadastro: DateTime(2020, 06, 15),
  ),
  NecessidadeModel(
    id: 1,
    descricao: 'Achocolatado',
    quantidade: 4,
    categoriaId: 1,
    usuarioId: 2,
    statusDoacaoId: 1,
    dataDeCadastro: DateTime(2020, 06, 15),
  ),
  NecessidadeModel(
    id: 1,
    descricao: 'Achocolatado',
    quantidade: 4,
    categoriaId: 1,
    usuarioId: 1,
    statusDoacaoId: 1,
    dataDeCadastro: DateTime(2020, 06, 15),
  ),
];
