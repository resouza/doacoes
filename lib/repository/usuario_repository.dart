import 'package:donate_app/models/usuario_model.dart';

class UsuarioRepository {
  static UsuarioModel getUserById(int usuarioId) {
    return usuarios.firstWhere((element) => element.id == usuarioId);
  }
}

List<UsuarioModel> usuarios = <UsuarioModel>[
  UsuarioModel(
    id: 1,
    nome: 'Bruno Oliveira',
    email: 'bruno@gmail.com',
    authId: 1,
    telefone: '(71) 99955-1203',
    ativo: true,
    dataCadastro: DateTime.now(),
  ),
  UsuarioModel(
    id: 1,
    nome: 'Bruno Oliveira',
    email: 'bruno@gmail.com',
    authId: 1,
    telefone: '(71) 99955-1203',
    ativo: true,
    dataCadastro: DateTime.now(),
  ),
  UsuarioModel(
    id: 2,
    nome: 'Bruno Oliveira',
    email: 'bruno@gmail.com',
    authId: 1,
    telefone: '(71) 99955-1203',
    ativo: true,
    dataCadastro: DateTime.now(),
  ),
];
