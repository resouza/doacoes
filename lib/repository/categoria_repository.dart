import 'package:donate_app/models/categoria_model.dart';

class CategoriaRepository {
  static List<CategoriaModel> getAllCategoria() {
    return categoriaList;
  }

  static CategoriaModel getCategoriaById(int id) {
    return categoriaList.firstWhere((categoria) => categoria.id == id);
  }
}

List<CategoriaModel> categoriaList = const [
  CategoriaModel(
    id: 1,
    nome: 'Alimento (não perecível)',
    ativo: true,
  ),
  CategoriaModel(
    id: 2,
    nome: 'Higiene Pessoal',
    ativo: true,
  ),
  CategoriaModel(
    id: 3,
    nome: 'Produtos de Limpeza',
    ativo: true,
  ),
  CategoriaModel(
    id: 4,
    nome: 'Material de Contrução',
    ativo: true,
  ),
  CategoriaModel(
    id: 5,
    nome: 'Vestuário',
    ativo: true,
  ),
  CategoriaModel(
    id: 6,
    nome: 'Calçados',
    ativo: true,
  ),
  CategoriaModel(
    id: 7,
    nome: 'Eletrônicos',
    ativo: true,
  ),
  CategoriaModel(
    id: 8,
    nome: 'Móveis',
    ativo: true,
  ),
  CategoriaModel(
    id: 9,
    nome: 'Cama, Mesa e Banho',
    ativo: true,
  ),
  CategoriaModel(
    id: 10,
    nome: 'Utensílios Domésticos',
    ativo: true,
  ),
  CategoriaModel(
    id: 11,
    nome: 'Livros / Material Escolar',
    ativo: true,
  ),
  CategoriaModel(
    id: 12,
    nome: 'Brinquedos',
    ativo: true,
  ),
];
