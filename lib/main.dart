import 'package:donate_app/core/app_routers.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const AppWidget());
}

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Donate App',
      debugShowCheckedModeBanner: false,
      initialRoute: signInRoute,
      onGenerateRoute: AppRouters.generateRoute,
    );
  }
}
